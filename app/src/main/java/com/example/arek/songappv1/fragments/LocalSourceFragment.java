package com.example.arek.songappv1.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.arek.songappv1.FilterType;
import com.example.arek.songappv1.R;
import com.example.arek.songappv1.adapters.LocalSongsRecyclerAdapter;
import com.example.arek.songappv1.presenters.LocalSongsFragmentPresenter;
import com.example.arek.songappv1.repositories.MainSongsRepository;
import com.example.arek.songappv1.views.SongsActivityRowView;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by arek on 7/31/2017.
 */

public class LocalSourceFragment extends Fragment implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {

    private LocalSongsFragmentPresenter presenter;
    private Context context;
    SongsActivityRowView rowView;
    @BindView(R.id.search_text)
    EditText searchEditText;
    @BindView(R.id.search_via_radio_group)
    RadioGroup searchViaRadioGroup;
    @BindView(R.id.local_rv)
    RecyclerView recyclerView;
    private FilterType filterType;
    @BindView(R.id.search_via_label)
    TextView searchViaLabel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_local, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getActivity().getApplicationContext();
        filterType = FilterType.ARTIST_NAME;
        initRecyclerView(context);
        implementEvents();
    }

    public void initRecyclerView(Context context) {

        presenter = new LocalSongsFragmentPresenter(rowView, new MainSongsRepository(context));
        LocalSongsRecyclerAdapter adapter = new LocalSongsRecyclerAdapter(presenter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {

        int pos = radioGroup.indexOfChild(radioGroup.findViewById(checkedId));//get the checked position of radio button

        switch (radioGroup.getId()) {
            case R.id.search_via_radio_group:
                switch (pos) {
                    case 0:
                        filterType = FilterType.ARTIST_NAME;
                        break;
                    case 1:
                        filterType = FilterType.SONG_NAME;
                        break;
                }
                break;
        }

    }

    public void implementEvents() {

        searchViaRadioGroup.setOnCheckedChangeListener(this);
        searchViaLabel.setOnClickListener(this);

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                presenter.filterLocal(filterType, charSequence.toString());
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.search_via_label:
                //show hide the radio group
                if (searchViaRadioGroup.isShown()) {
                    searchViaLabel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_dropdown, 0);
                    searchViaRadioGroup.setVisibility(View.GONE);
                } else {
                    searchViaLabel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_dropdown, 0);
                    searchViaRadioGroup.setVisibility(View.VISIBLE);
                }
                break;

        }
    }
}
