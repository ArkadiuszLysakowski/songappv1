package com.example.arek.songappv1.repositories;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.example.arek.songappv1.ItunesCallback;
import com.example.arek.songappv1.models.SongModel;
import com.example.arek.songappv1.models.Result;
import com.example.arek.songappv1.rest.ApiAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.arek.songappv1.models.SongModelLocal;
import com.example.arek.songappv1.rest.ApiInterface;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by arek on 7/31/2017.
 */

public class MainSongsRepository implements SongsRepository {

    ItunesCallback callback;
    private static final String TAG = "Utils";
    Context context;
    ApiInterface mApiInterface;
    private List<SongModel> resultEntries;
    List<SongModelLocal> songsList;
    List<SongModel> songModelEntries;
    Result resultList;
    private Realm realm;

    public MainSongsRepository(Context context) {
        this.context = context;
    }
    @Override
    public List<SongModelLocal> getSongs() {
        try{
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            JSONArray array = new JSONArray(loadJSONFromAsset(context, "songs-list.json"));
            songsList = new ArrayList<>();
            for(int i=0;i<array.length();i++){
                SongModelLocal profile = gson.fromJson(array.getString(i), SongModelLocal.class);
                songsList.add(profile);
            }
            return songsList;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onSuccessResponse(List<SongModel> songModels) {
//        songModels = new ArrayList<>();
        Log.d("", String.valueOf(songModels));
    }

    public void loadSongsFromApi(final ItunesCallback callback){

       resultList = new Result();
       songModelEntries = new ArrayList<>();

//       final List<SongModel> songModelList = new ArrayList<SongModel>();

        mApiInterface = ApiAdapter.getAPIService();
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("amgArtistId", "468749,5723");
        queryMap.put("entity", "song");
        queryMap.put("limit", "5");
        queryMap.put("sort", "recent");
//
        resultEntries = new ArrayList<SongModel>();

        mApiInterface.loadSongs(queryMap).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Log.d(" ", response.body().toString());
                Result itunesResult = response.body();
                List<SongModel> results = itunesResult.getResults();
                resultEntries.addAll(results);
                callback.onSuccessResponse(results);
            }
            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
//        Log.d("", String.valueOf(resultEntries.size()));

    }
    private static String loadJSONFromAsset(Context context, String jsonFileName){

        String json = null;
        InputStream is=null;

        try {
            AssetManager manager = context.getAssets();
            Log.d(TAG,"path "+jsonFileName);
            is = manager.open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
