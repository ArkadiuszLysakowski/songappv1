package com.example.arek.songappv1.rest;


import com.example.arek.songappv1.rest.Services.ServiceGenerator;

/**
 * Created by Arek on 2017-02-08.
 */

public class ApiAdapter {

    private ApiAdapter() {}

    public static ApiInterface getAPIService() {

        return ServiceGenerator.getClient(IntentKeys.BASE_URL).create(ApiInterface.class);
    }

}
