package com.example.arek.songappv1.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.arek.songappv1.FilterType;
import com.example.arek.songappv1.R;
import com.example.arek.songappv1.presenters.RemoteSongsFragmentPresenter;
import com.example.arek.songappv1.adapters.RemoteSongsRecyclerAdapter;
import com.example.arek.songappv1.models.Result;
import com.example.arek.songappv1.models.SongModel;
import com.example.arek.songappv1.rest.ApiAdapter;
import com.example.arek.songappv1.rest.ApiInterface;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by arek on 8/3/2017.
 */

public class RemoteSourceFragment extends Fragment implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {

    private RemoteSongsFragmentPresenter presenter;
    private Context context;
    private RemoteSongsRecyclerAdapter adapter;
    @BindView(R.id.search_text)
    EditText searchEditText;
    @BindView(R.id.search_via_radio_group)
    RadioGroup searchViaRadioGroup;
    @BindView(R.id.remote_rv)
    RecyclerView mRecyclerView;
    @BindView(R.id.search_via_label)
    TextView searchViaLabel;
    ApiInterface mApiInterface;
    private FilterType filterType;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_remote, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        context = getActivity().getApplicationContext();
        initRecyclerView(context);
        loadJsonFromApi();
        filterType = FilterType.ARTIST_NAME;
        implementEvents();
    }

    private void loadJsonFromApi() {

        mApiInterface = ApiAdapter.getAPIService();
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("amgArtistId", "468749,5723");
        queryMap.put("entity", "song");
        queryMap.put("limit", "10");
        queryMap.put("sort", "recent");

        mApiInterface.loadSongs(queryMap).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Log.d(" ", response.body().toString());
                Result itunesResult = response.body();
                List<SongModel> results = itunesResult.getResults();
                adapter = new RemoteSongsRecyclerAdapter(results);
                mRecyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });

    }
    private void initRecyclerView(Context context) {

//        presenter = new RemoteSongsFragmentPresenter(rowView, new MainSongsRepository(context));
//        RemoteSongsRecyclerAdapter adapter = new RemoteSongsRecyclerAdapter(presenter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.search_via_label:
                //show hide the radio group
                if (searchViaRadioGroup.isShown()) {
                    searchViaLabel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_dropdown, 0);
                    searchViaRadioGroup.setVisibility(View.GONE);
                } else {
                    searchViaLabel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_dropdown, 0);
                    searchViaRadioGroup.setVisibility(View.VISIBLE);
                }
                break;

        }

    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {

        int pos = radioGroup.indexOfChild(radioGroup.findViewById(checkedId));

        switch (radioGroup.getId()) {
            case R.id.search_via_radio_group:
                switch (pos) {
                    case 0:
                        filterType = FilterType.ARTIST_NAME;
                        break;
                    case 1:
                        filterType = FilterType.SONG_NAME;
                        break;
                }
                break;
        }

    }

    public void implementEvents() {

        searchViaRadioGroup.setOnCheckedChangeListener(this);
        searchViaLabel.setOnClickListener(this);

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                adapter.filterLocal(filterType, charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

    }
}