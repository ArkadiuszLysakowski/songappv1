package com.example.arek.songappv1.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.arek.songappv1.FilterType;
import com.example.arek.songappv1.R;
import com.example.arek.songappv1.presenters.RemoteSongsFragmentPresenter;
import com.example.arek.songappv1.models.SongModel;
import com.example.arek.songappv1.repositories.MainSongsRepository;
import com.example.arek.songappv1.views.SongsActivityRowView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by arek on 8/3/2017.
 */

public class RemoteSongsRecyclerAdapter extends RecyclerView.Adapter<RemoteSongsRecyclerAdapter.RemoteSongsViewHolder> {

    List<SongModel> originalSongsList;
    List<SongModel> filteredSongsList;
    MainSongsRepository repository;
    RemoteSongsFragmentPresenter presenter;
    private Context context;


    public RemoteSongsRecyclerAdapter(List<SongModel> originalSongsList) {
        this.context = context;
        this.originalSongsList = originalSongsList;
        this.filteredSongsList = new CopyOnWriteArrayList<>();
        this.filteredSongsList.addAll(originalSongsList);

    }

//    public RemoteSongsRecyclerAdapter(RemoteSongsFragmentPresenter presenter) {
//        this.presenter = presenter;
//    }

    @Override
    public RemoteSongsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_remote, parent, false);
        return new RemoteSongsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RemoteSongsViewHolder holder, int position) {

//        presenter.onBindSongsActivityRowViewAtPositionFromRemote(position, holder);
        SongModel model = originalSongsList.get(position);
        holder.setSongName(model.getTrackName());
        holder.setReleaseYear(model.getReleaseDate());
        holder.setArtist(model.getArtistName());

    }

    @Override
    public int getItemCount() {
        return originalSongsList.size();
    }

    public class RemoteSongsViewHolder extends RecyclerView.ViewHolder implements SongsActivityRowView {

        @BindView(R.id.txt_song_name)
        TextView songNameTextView;
        @BindView(R.id.txt_artist_name)
        TextView artistNameTextView;
        @BindView(R.id.txt_release_year)
        TextView releaseYearTextView;

        public RemoteSongsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void setSongName(String title) {
            songNameTextView.setText(title);
        }

        @Override
        public void setArtist(String artist) {
            artistNameTextView.setText(artist);
        }

        @Override
        public void setReleaseYear(String releaseYear) {
            releaseYearTextView.setText(releaseYear);
        }
    }


    public void filterLocal(FilterType filterType, String charText) {

        if (filterType == FilterType.SONG_NAME || filterType == FilterType.ARTIST_NAME) {
            charText = charText.toLowerCase(Locale.getDefault());
        }
        originalSongsList.clear();

        if (charText.length() == 0) {
            originalSongsList.add((SongModel) filteredSongsList);
        } else {

            for (SongModel song : filteredSongsList) {

                switch (filterType) {
                    case ARTIST_NAME:
                        if (song.getArtistName().toLowerCase(Locale.getDefault()).contains(charText))
                            originalSongsList.add(song);
                        break;
                    case SONG_NAME:
                        if (song.getTrackName().toLowerCase(Locale.getDefault()).contains(charText))
                            originalSongsList.add(song);
                        break;
                }
//                filteredSongsList.remove(song.toString());
            }
        }

        notifyDataSetChanged();
    }

}
