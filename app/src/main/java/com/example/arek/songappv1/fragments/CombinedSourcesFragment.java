package com.example.arek.songappv1.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.arek.songappv1.R;
import com.example.arek.songappv1.adapters.LocalSongsRecyclerAdapter;
import com.example.arek.songappv1.presenters.LocalSongsFragmentPresenter;
import com.example.arek.songappv1.adapters.RemoteSongsRecyclerAdapter;
import com.example.arek.songappv1.models.Result;
import com.example.arek.songappv1.models.SongModel;
import com.example.arek.songappv1.repositories.MainSongsRepository;
import com.example.arek.songappv1.rest.ApiAdapter;
import com.example.arek.songappv1.rest.ApiInterface;
import com.example.arek.songappv1.views.SongsActivityRowView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by arek on 8/4/2017.
 */

public class CombinedSourcesFragment extends Fragment {

    private Context context;
    private LocalSongsFragmentPresenter presenter;
    @BindView(R.id.remote)
    RecyclerView mRecyclerRemote;

    @BindView(R.id.local)
    RecyclerView mRecyclerLocal;

    SongsActivityRowView rowView;
    private RemoteSongsRecyclerAdapter remoteAdapter;
    ApiInterface mApiInterface;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_combined, container, false);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        setHasOptionsMenu(true);
        context = getActivity().getApplicationContext();
        initLocalRecyclerView(context);
        initRemoteRecycleView(context);

    }

    public void initLocalRecyclerView(Context context){

        presenter = new LocalSongsFragmentPresenter(rowView, new MainSongsRepository(context));
        LocalSongsRecyclerAdapter adapter = new LocalSongsRecyclerAdapter(presenter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        mRecyclerLocal.setLayoutManager(layoutManager);
        mRecyclerLocal.setHasFixedSize(true);
        mRecyclerLocal.setAdapter(adapter);
    }

    public void initRemoteRecycleView(Context context){

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        mRecyclerRemote.setLayoutManager(layoutManager);
        mRecyclerRemote.setHasFixedSize(true);
        loadJsonFromApi();

    }

    private void loadJsonFromApi(){

        mApiInterface = ApiAdapter.getAPIService();
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("amgArtistId", "468749,5723");
        queryMap.put("entity", "song");
        queryMap.put("limit", "10");
        queryMap.put("sort", "recent");


        mApiInterface.loadSongs(queryMap).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Log.d(" ", response.body().toString());
                Result itunesResult = response.body();
                List<SongModel> results = itunesResult.getResults();
                remoteAdapter = new RemoteSongsRecyclerAdapter(results);
                mRecyclerRemote.setAdapter(remoteAdapter);
            }
            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });

}
}
