package com.example.arek.songappv1;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.arek.songappv1.adapters.ViewPagerAdapter;
import com.example.arek.songappv1.models.SongModel;
import com.example.arek.songappv1.repositories.MainSongsRepository;

import org.apache.log4j.chainsaw.Main;

import java.util.List;

public class SongsActivity extends AppCompatActivity {

    TabLayout tablayout;
    ViewPager viewpager;
    ViewPagerAdapter viewPagerAdapter;
    MainSongsRepository repository;
    List<SongModel> models;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initViewPager();

    }

    private void initViewPager() {
        viewpager = (ViewPager) findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewpager.setAdapter(viewPagerAdapter);
        tablayout = (TabLayout) findViewById(R.id.tabs);
        tablayout.setupWithViewPager(viewpager);
    }
}
