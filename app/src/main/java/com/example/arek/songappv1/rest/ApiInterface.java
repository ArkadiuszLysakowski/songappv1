package com.example.arek.songappv1.rest;

import java.util.Map;

import com.example.arek.songappv1.models.Result;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by Arek on 2017-02-08.
 */

public interface ApiInterface {

    @GET("/lookup")
    Call<Result> loadSongs(@QueryMap Map<String, String> options);

//    @GET("/lookup")
//    Observable<Result> loadSongs(@QueryMap Map<String, String> options);

}
