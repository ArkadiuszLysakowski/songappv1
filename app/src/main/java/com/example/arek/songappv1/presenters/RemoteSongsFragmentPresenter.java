package com.example.arek.songappv1.presenters;
import com.example.arek.songappv1.ItunesCallback;
import com.example.arek.songappv1.models.SongModel;
import com.example.arek.songappv1.repositories.MainSongsRepository;
import com.example.arek.songappv1.views.SongsActivityRowView;

import java.util.List;

/**
 * Created by arek on 8/3/2017.
 */

public class RemoteSongsFragmentPresenter implements ItunesCallback {

    private SongsActivityRowView view;
    private MainSongsRepository repository;

    List<SongModel> remoteSongModelList;
    List<SongModel> test;

    public RemoteSongsFragmentPresenter(SongsActivityRowView view, MainSongsRepository mLocalRepository) {
        this.view = view;
        this.repository = mLocalRepository;
//        repository.loadSongsFromApi(new ItunesCallback() {
//            @Override
//            public void onSuccessResponse(List<SongModel> songModels) {
//                test = songModels;
//            }
//        });
    }

    public void onBindSongsActivityRowViewAtPositionFromRemote(int position, SongsActivityRowView rowView){


//        SongModel songModel = test.get(position);
        SongModel songModel = remoteSongModelList.get(position);
        rowView.setSongName(songModel.getTrackName());
        rowView.setArtist(songModel.getArtistName());
        rowView.setReleaseYear(songModel.getReleaseDate());
    }

    public int getRemoteRowsCount() {
        return (null != test ? test.size() : 0);
    }
//(null != test ? test.size() : 0)
    @Override
    public void onSuccessResponse(List<SongModel> songModels) {

    }
}
