package com.example.arek.songappv1.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by arek on 7/30/2017.
 */

public class SongModelLocal {

    @SerializedName("Song Clean")
    String mSongName;
    @SerializedName("ARTIST CLEAN")
    String mArtistName;
    @SerializedName("Release Year")
    String mReleaseYear;
    @SerializedName("COMBINED")
    String mCombined;
    @SerializedName("First?")
    int mFirst;
    @SerializedName("Year?")
    int mYear;

    public String getmSongName() {
        return mSongName;
    }

    public void setmSongName(String mSongName) {
        this.mSongName = mSongName;
    }

    public String getmArtistName() {
        return mArtistName;
    }

    public void setmArtistName(String mArtistName) {
        this.mArtistName = mArtistName;
    }

    public String getmReleaseYear() {
        return mReleaseYear;
    }

    public void setmReleaseYear(String mReleaseYear) {
        this.mReleaseYear = mReleaseYear;
    }

    public String getmCombined() {
        return mCombined;
    }

    public void setmCombined(String mCombined) {
        this.mCombined = mCombined;
    }
}
