package com.example.arek.songappv1;

import com.example.arek.songappv1.models.SongModel;

import java.util.List;

/**
 * Created by arek on 8/4/2017.
 */

public interface ItunesCallback {

    void onSuccessResponse(List<SongModel> songModels);

}
