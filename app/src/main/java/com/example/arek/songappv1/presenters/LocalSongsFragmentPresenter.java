package com.example.arek.songappv1.presenters;

import android.util.Log;

import com.example.arek.songappv1.FilterType;
import com.example.arek.songappv1.adapters.LocalSongsRecyclerAdapter;
import com.example.arek.songappv1.models.SongModelLocal;
import com.example.arek.songappv1.repositories.MainSongsRepository;
import com.example.arek.songappv1.views.SongsActivityRowView;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by arek on 7/30/2017.
 */

public class LocalSongsFragmentPresenter {

    private SongsActivityRowView view;
    private MainSongsRepository mLocalRepository;
    List<SongModelLocal> originalSongsList;
    List<SongModelLocal> filteredSongsList;
    LocalSongsRecyclerAdapter adapter;

    public LocalSongsFragmentPresenter(SongsActivityRowView view, MainSongsRepository mLocalRepository) {
        this.view = view;
        this.mLocalRepository = mLocalRepository;
        originalSongsList = mLocalRepository.getSongs();
        this.filteredSongsList = new CopyOnWriteArrayList<>();
        this.filteredSongsList.addAll(originalSongsList);
    }

    public void onBindSongsActivityRowViewAtPosition(int position, SongsActivityRowView rowView){

            SongModelLocal songModelLocal = originalSongsList.get(position);
            rowView.setSongName(songModelLocal.getmSongName());
            rowView.setArtist(songModelLocal.getmArtistName());
            rowView.setReleaseYear(songModelLocal.getmReleaseYear());
        }
//(null != test ? test.size() : 0)
    public int getSongsRowsCount() {
        return (null != originalSongsList ? originalSongsList.size() : 0);
    }

    public List<SongModelLocal> getSongs(){

        return originalSongsList;
    }

    public void filterLocal(FilterType filterType, String charText){

        adapter = new LocalSongsRecyclerAdapter();

        if(filterType == FilterType.SONG_NAME || filterType == FilterType.ARTIST_NAME){
            charText = charText.toString().toLowerCase();
        }

        originalSongsList.clear();

        if (charText.length() == 0){
            originalSongsList.addAll(filteredSongsList);
        }else {

            for (SongModelLocal songModelLocal : filteredSongsList){

                Log.d("", songModelLocal.toString());
                Log.d("", songModelLocal.getmSongName().toString());
                switch (filterType){
                    case ARTIST_NAME:
                        if (songModelLocal.getmArtistName().toLowerCase(Locale.getDefault()).contains(charText))
                            originalSongsList.add(songModelLocal);
                        break;
                    case SONG_NAME:
                        if (songModelLocal.getmSongName().toLowerCase(Locale.getDefault()).contains(charText))
                            originalSongsList.add(songModelLocal);
                        break;
                }
//                filteredSongsList.remove(songModelLocal.toString());
            }
        }
//        originalSongsList.notify();
        adapter.notifyDataSetChanged();
    }

}
