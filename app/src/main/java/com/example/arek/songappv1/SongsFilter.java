package com.example.arek.songappv1;

import com.example.arek.songappv1.adapters.LocalSongsRecyclerAdapter;
import com.example.arek.songappv1.adapters.RemoteSongsRecyclerAdapter;
import com.example.arek.songappv1.models.SongModelLocal;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by arek on 8/6/2017.
 */

public class SongsFilter {

    private LocalSongsRecyclerAdapter mLocalAdapter;
    private RemoteSongsRecyclerAdapter mRemoteAdapter;
    private final ArrayList<SongModelLocal> mOriginalList;
    private ArrayList<SongModelLocal> mFilteredList;
    FilterType mFilterType;

    private SongsFilter(LocalSongsRecyclerAdapter adapter, ArrayList<SongModelLocal> originalList, ArrayList<SongModelLocal> filteredList, FilterType filterType){

        this.mLocalAdapter = adapter;
        this.mOriginalList = originalList;
        this.mFilteredList = filteredList;
        this.mFilterType = filterType;
    }

    public SongsFilter(RemoteSongsRecyclerAdapter mRemoteAdapter, ArrayList<SongModelLocal> mOriginalList, ArrayList<SongModelLocal> mFilteredList, FilterType mFilterType) {
        this.mRemoteAdapter = mRemoteAdapter;
        this.mOriginalList = mOriginalList;
        this.mFilteredList = mFilteredList;
        this.mFilterType = mFilterType;
    }

    public ArrayList<SongModelLocal> filterLocal(FilterType filterType, String charText){

        if(filterType == FilterType.SONG_NAME || filterType == FilterType.ARTIST_NAME){
            charText = charText.toString().toLowerCase();
        }

        if (charText.length() == 0){
            mFilteredList = mOriginalList;
        }else {

            for (SongModelLocal songModelLocal : mOriginalList){

                switch (filterType){

                    case ARTIST_NAME:
                        if (songModelLocal.getmArtistName().toLowerCase(Locale.getDefault()).contains(charText))
                            mFilteredList.add(songModelLocal);
                        break;
                    case SONG_NAME:
                        if (songModelLocal.getmSongName().toLowerCase().contains(charText))
                            mFilteredList.add(songModelLocal);
                        break;
                }

            }
        }
        return mFilteredList;
    }


//    @Override
//    protected FilterResults performFiltering(CharSequence charSequence) {
//        mFilteredList.clear();
//
//        String charString = charSequence.toString();
//
//        if (charString.isEmpty()) {
//
//            mFilteredList = mOriginalList;
//
//        } else {
//
//            ArrayList<SongModelLocal> filteredList = new ArrayList<>();
//
//            for (SongModelLocal song : mOriginalList) {
//
//                if (song.getmArtistName().toLowerCase().contains(charString) || song.getmSongName().toLowerCase().contains(charString)) {
//
//                    filteredList.add(song);
//                }
//            }
//            mFilteredList = mOriginalList;
//        }
//
//        FilterResults filterResults = new FilterResults();
//        filterResults.values = mFilteredList;
//        return filterResults;
//    }
//
//    @Override
//    protected void publishResults(CharSequence constraint, FilterResults results) {
//
//        mLocalAdapter.notifyDataSetChanged();
//
//    }
}
