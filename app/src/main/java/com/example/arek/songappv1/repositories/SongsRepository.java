package com.example.arek.songappv1.repositories;

import java.util.List;

import com.example.arek.songappv1.models.SongModel;
import com.example.arek.songappv1.models.SongModelLocal;

/**
 * Created by arek on 7/30/2017.
 */

public interface SongsRepository {

    List<SongModelLocal> getSongs();
    void onSuccessResponse(List<SongModel> songModels);

}
