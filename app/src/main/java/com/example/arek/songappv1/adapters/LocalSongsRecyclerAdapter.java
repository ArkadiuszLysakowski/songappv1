package com.example.arek.songappv1.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.arek.songappv1.R;
import com.example.arek.songappv1.presenters.LocalSongsFragmentPresenter;
import com.example.arek.songappv1.views.SongsActivityRowView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by arek on 7/30/2017.
 */

public class LocalSongsRecyclerAdapter extends RecyclerView.Adapter<LocalSongsRecyclerAdapter.SongsViewHolder>{

    private LocalSongsFragmentPresenter presenter;

    public LocalSongsRecyclerAdapter(LocalSongsFragmentPresenter localSongsFragmentPresenter) {
        this.presenter = localSongsFragmentPresenter;
    }

    public LocalSongsRecyclerAdapter(){

    }

    @Override
    public SongsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_local, parent, false);
       return new SongsViewHolder(view);

    }

    @Override
    public void onBindViewHolder(SongsViewHolder holder, int position) {

        presenter.onBindSongsActivityRowViewAtPosition(position,holder);
    }

    @Override
    public int getItemCount() {
        return presenter.getSongsRowsCount();
    }



    public class SongsViewHolder extends RecyclerView.ViewHolder implements SongsActivityRowView{

        @BindView(R.id.txt_song_name)
        TextView songNameTextView;
        @BindView(R.id.txt_artist_name)
        TextView artistNameTextView;
        @BindView(R.id.txt_release_year)
        TextView releaseYearTextView;

        public SongsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void setSongName(String title) {
            songNameTextView.setText(title);
        }

        @Override
        public void setArtist(String artist) {
            artistNameTextView.setText(artist);
        }

        @Override
        public void setReleaseYear(String releaseYear) {
            releaseYearTextView.setText(releaseYear);
        }
    }
}


