package com.example.arek.songappv1.views;

/**
 * Created by arek on 7/30/2017.
 */

public interface SongsActivityRowView {

    void setSongName(String title);
    void setArtist(String artist);
    void setReleaseYear(String releaseYear);

}
