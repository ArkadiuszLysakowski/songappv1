package com.example.arek.songappv1.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arek on 8/2/2017.
 */

public class Result {

    private Integer resultCount;
    private List<SongModel> results = new ArrayList<SongModel>();

    public Integer getResultCount() {
        return resultCount;
    }

    public List<SongModel> getResults() {
        return results;
    }


}
