package com.example.arek.songappv1.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.arek.songappv1.fragments.CombinedSourcesFragment;
import com.example.arek.songappv1.fragments.LocalSourceFragment;
import com.example.arek.songappv1.fragments.RemoteSourceFragment;

/**
 * Created by arek on 7/31/2017.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private String ALL = "All";
    private String LOCAL = "Local";
    private String REMOTE = "Remote";

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new CombinedSourcesFragment();
        }
        else if (position == 1)
        {
            fragment = new LocalSourceFragment();
        }
        else if (position == 2)
        {
            fragment = new RemoteSourceFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        String title = null;

        if (position == 0)
        {
            title = ALL;
        }
        else if (position == 1)
        {
            title = LOCAL;
        }
        else if (position == 2)
        {
            title = REMOTE;
        }
        return title;

    }
}
