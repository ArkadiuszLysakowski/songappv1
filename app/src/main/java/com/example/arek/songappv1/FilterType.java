package com.example.arek.songappv1;

/**
 * Created by arek on 8/6/2017.
 */

public enum FilterType {

    SONG_NAME, ARTIST_NAME;

}
